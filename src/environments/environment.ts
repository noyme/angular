// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCMznNdJyE4N22I-wsfZxBNtJW8Dn9-yM8",
    authDomain: "hello-noy-jce.firebaseapp.com",
    databaseURL: "https://hello-noy-jce.firebaseio.com",
    projectId: "hello-noy-jce",
    storageBucket: "hello-noy-jce.appspot.com",
    messagingSenderId: "853587329910",
    appId: "1:853587329910:web:c9cfd9ac4802d53a94b057"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
