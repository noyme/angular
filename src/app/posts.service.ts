import { PostRaw } from './interfaces/post-raw';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  private URL :string = "https://jsonplaceholder.typicode.com/posts/";

  constructor(private http:HttpClient) { }

  getPost():Observable<PostRaw>{
    return this.http.get<PostRaw>(`${this.URL}`);
  }

}
