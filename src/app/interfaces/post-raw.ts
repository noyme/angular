export interface PostRaw {

    title: string,
    body: string,
    id: number,
    userId: number
}
