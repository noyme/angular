import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {

  
  blabla;
  

  seasons: string[] = ['bbc', 'cnn', 'nbc'];

  constructor(private route:ActivatedRoute) { }

  ngOnInit(): void {
    var blabla = this.route.snapshot.params.blabla; 
    this.blabla = blabla.toLowerCase()
  }
}