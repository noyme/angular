import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email:string;
  password:string; 
  errorMessage:string; 
  isError:boolean = false;

  constructor(private auth:AuthService, private router:Router) { }

  onSubmit(){
    this.auth.login(this.email, this.password)
      .then(res => {
      console.log(res);
      this.router.navigate(['/books']);
      }).catch(err => {
        console.log(err);
        this.isError = true; 
        this.errorMessage = err.message; 
      });
  }

  ngOnInit(): void {
  }

}
