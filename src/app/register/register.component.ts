import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  email:string;
  password:string;



  constructor(public auth:AuthService, private router:Router) { }

  onSubmit(){
    this.auth.register(this.email, this.password)
    .then((result) => {
      window.alert("You have been successfully registered!");
      console.log(result.user);
      this.router.navigate(['/books']);
      }).catch((error) => {
        window.alert(error.message)
      });
  }

  ngOnInit(): void {
  }

}
