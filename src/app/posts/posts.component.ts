import { PostRaw } from './../interfaces/post-raw';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  posts;
  postData$:Observable<PostRaw>;

  constructor( private PostService:PostsService) { }

  ngOnInit(): void {
    this.postData$ = this.PostService.getPost();
    this.postData$.subscribe(data => this.posts = data);
  }

}
